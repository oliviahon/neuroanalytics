#!/bin/bash
cd donorfiles/ #go to the folder that my data is in
for i in `seq 1 50` #i refers to donor ID
do
for j in `seq 1 10` #j refers to timepont id
do
chmod a-w donor${i}_tp${j}.txt #remove write permissions for all
done
done


mkdir fakedata #make fake data directory
cd fakedata/ #go int fakedata directory
for i in `seq 1 50`
do
mkdir donor${i} #make directory for each donor within fakedata
done

cd .. #go up one directory  back into donofiles director
for i in `seq 1 50`
do
for j in `seq 1 10`
do
mv donor${i}_tp${j}.txt fakedata/donor${i}/ #find all files for each donor and move into their folder
done
done
 
