#!/bin/bash
basename /nas/longleaf/home/ohon/hwtest.txt #hapmap1.ped; #reads in file and names it as a variable
numberoflines=`wc -l < hwtest.txt` #hapmap1.ped #creates the variable numberoflines. assigns it the output of the
#function. wc -l gives the number of lines, and returns the number without the filename

for i in `seq 1 ${numberoflines}`
do 
head -n ${i} hwtest.txt |tail -1 #gives the first i number of lines, pipes into tail so we only get the last one
sleep 1;# wait 1 second
done

