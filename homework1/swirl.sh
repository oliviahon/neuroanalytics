#!/bin/bash
for i in `seq 1 5`
do
echo -ne '\b\\'; # -n suppresses trailing line so that when new character pops up 
# it comes up on the same line
sleep .5; #waits .5 seconds
echo -ne '\b|'; #-e allows for the interpretatin of other backslashed characters
#\b displays backspace character, so deletes the first backslash and then displa#ys |
sleep .5;
echo -ne '\b/' #deletes that and then displays / 
sleep .5;
echo -ne '\b-'; #deletes and displays -
sleep .5;
echo -ne '\b\\';
sleep .5;
echo -ne '\b|';
sleep .5;
echo -ne '\b/';
sleep .5;
echo -ne '\b-';
sleep .5;
done
echo -ne "\b \n"; #\n creates a new line

for i in `seq 1 10` #leftward spin
do 
echo -ne '\b>'; #pops up >
sleep .5
echo -ne '\b^'; # deletes > and returns ^
sleep .5
echo -ne '\b<'; #deletes ^ and returns <
sleep .5
done
echo -ne "\b \n"; #deletes < and starts new line

for i in `seq 1 10` #rightward spin
do 
echo -ne '\b<'; #pops up >
sleep .5
echo -ne '\b^'; # deletes > and returns ^
sleep .5
echo -ne '\b>'; #deletes ^ and returns <
sleep .5
done
echo -ne "\b \n"; #deletes < and starts new line

for i in `seq 1 10` #up and down flip
do 
echo -ne '\bp'; #pops up p
sleep .5
echo -ne '\b*'; # deletes p  and returns *
sleep .5
echo -ne '\bb'; #deletes *  and returns b
sleep .5
echo -ne '\b*'; #deletes b and returns *
done
echo -ne "\b \n"; #deletes and starts a new line

for i in `seq 1 10` #down then up flip
do 
echo -ne '\bb'; #pops up b
sleep .5
echo -ne '\b*'; # deletes b and returns *
sleep .5
echo -ne '\bp'; #deletes * and returns p
sleep .5
echo -ne '\b*'; #deletes p and returns *
done
echo -ne "\b \n"; #deletes and starts a new line
